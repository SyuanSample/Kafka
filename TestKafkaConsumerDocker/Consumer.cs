﻿using Confluent.Kafka;
using DomainModel.Const;
using Serilog;

namespace TestKafkaConsumerDocker;

public class Consumer
{
    private readonly ConsumerConfig _config;
    private          string         _name;

    public Consumer(string host)
    {
        _name = Environment.GetEnvironmentVariable(EnvKey.DockerName)!;

        _config = new ConsumerConfig
        {
            GroupId         = KafkaGroup.ProducerAndConsumer, BootstrapServers = host
          , AutoOffsetReset = AutoOffsetReset.Earliest
        };
    }

    public void CreateConsumer()
    {
        using var consumer = new ConsumerBuilder<long, string>(_config).Build();
        consumer.Subscribe(KafkaTopic.ProducerAndConsumer);
        var cts = new CancellationTokenSource();

        Console.CancelKeyPress += (_
                                 , e) =>
        {
            e.Cancel = true; // prevent the process from terminating.
            cts.Cancel();
        };

        while (true)
        {
            try
            {
                var cr          = consumer.Consume(cts.Token);
                var receiveTime = DateTime.Now;
                var sendTime    = new DateTime(cr.Message.Key);
                var diffTime    = receiveTime - sendTime;

                Log.Logger.Information
                (
                    "接收者{Name} - Topic:{ProducerAndConsumer},發送時間:{MessageValue}, 接收時間：{ReceiveTime}經過時間:{DiffTimeTotalSeconds}"
                  , _name, KafkaTopic.ProducerAndConsumer, cr.Message.Value, receiveTime, diffTime.TotalSeconds
                );
            }
            catch (ProduceException<long, string> e)
            {
                Log.Logger.Error
                (
                    "接收錯誤 - Topic:{ProducerAndConsumer}, Reason:{ErrorReason}, Error:{EError}"
                  , KafkaTopic.ProducerAndConsumer, e.Error.Reason, e.Error
                );
            }
        }
    }
}