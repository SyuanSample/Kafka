﻿using System.Net;
using Confluent.Kafka;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

Console.WriteLine("Program Start Up.");

#region LoggerSetting

Log.Logger = new LoggerConfiguration()
             .MinimumLevel.Information()
             .WriteTo.Seq("http://localhost:5341")
             .WriteTo.Console()
             .CreateLogger();

#endregion

using var host = Host.CreateDefaultBuilder(args)
                     .Build();

var config = new ProducerConfig { BootstrapServers = "192.168.0.159:9091" };

using var p = new ProducerBuilder<Null, string>(config).Build();

try
{
     var dr = await p.ProduceAsync("TestTopic", new Message<Null, string> { Value = "test" });
    // Console.WriteLine($"Delivered '{dr.Value}' to '{dr.TopicPartitionOffset}'");
}

catch (ProduceException<Null, string> e)
{
    Console.WriteLine($"Delivery failed: {e.Error.Reason}");
}