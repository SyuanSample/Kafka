﻿namespace DomainModel.Const;

public static class KafkaTopic
{
    public const string TestTopic = "TestTopic";
    public const string ProducerAndConsumer = "ProducerAndConsumer";
}