﻿namespace DomainModel.Const;

public static class EnvKey
{
    /// <summary>
    ///     Kafka 的 ENV
    /// </summary>
    public const string KafkaServiceIpPort = "KafkaServiceIpPort";

    /// <summary>
    ///     Kafka 的 ENVh
    /// </summary>
    public const string SeqService = "SeqService";

    /// <summary>
    ///     DockerName
    /// </summary>
    public const string DockerName = "DockerName";
}