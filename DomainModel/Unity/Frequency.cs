﻿namespace DomainModel.Unity;

public static class Frequency
{
    private const int oneSecond = 1000;
    private const int oneMin    = 60000;

    /// <summary>
    ///     取得每秒鐘為單位，要達到目標次數的每秒間隔。
    /// </summary>
    /// <param name="target">每秒執行次數</param>
    /// <returns>
    ///     millisecond
    /// </returns>
    public static int GetOneSecInterval(int target)
    {
        var result = oneSecond / target;

        return result;
    }

    /// <summary>
    ///     取得每分鐘為單位，要達到目標次數的每秒間隔。
    /// </summary>
    /// <param name="target">每分鐘執行次數</param>
    /// <returns>
    ///     millisecond
    /// </returns>
    public static int GetOneMinInterval(int target)
    {
        var result = oneMin / target;

        return result;
    }
}