﻿namespace DomainModel.Model;

/// <summary>
///     Kafka相關設定
/// </summary>
public class KafkaSetting
{
    /// <summary>
    ///     Kafka主機網址
    /// </summary>
    public string BootstrapServers { get; set; }
}