﻿using DomainModel.Const;
using DomainModel.Model;
using Serilog;
using TestKafkaProducerAndConsumer.Unity;

#region Log Area

Log.Logger = new LoggerConfiguration()
             .MinimumLevel.Information()
             .WriteTo.Seq("http://localhost:5341")
             .WriteTo.Console()
             .CreateLogger();

Log.Logger.Information("Log初始化完成，完成時間{Now}，開始主程式", DateTime.Now);

#endregion

var kafkaServer = Environment.GetEnvironmentVariable(EnvKey.KafkaServiceIpPort)!;
Log.Logger.Information("Kafka連線IP：{Kafka}", kafkaServer);
var kafka = new KafkaSetting()
{
    BootstrapServers = kafkaServer
};

var p = new Produce(kafka.BootstrapServers);
var c = new Consumer(kafka.BootstrapServers);

ThreadPool.SetMinThreads(256, 256);
//var consumer = new Thread( ()=> c.CreateConsumer());
var produce = new Thread( ()=>
    {
        p.CreateProducer();
    }
);

//consumer.Start();
produce.Start();

Console.WriteLine("End");

