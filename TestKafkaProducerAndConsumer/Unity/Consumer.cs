﻿using Confluent.Kafka;
using DomainModel.Const;
using Serilog;

namespace TestKafkaProducerAndConsumer.Unity;

public class Consumer
{
    private readonly ConsumerConfig _config;

    public Consumer(string host)
    {
        _config = new ConsumerConfig
        {
            GroupId         = KafkaGroup.ProducerAndConsumer, BootstrapServers = host
          , AutoOffsetReset = AutoOffsetReset.Earliest
        };

    }

    public bool CreateConsumer()
    {
        using var consumer = new ConsumerBuilder<long, string>(_config).Build();
        consumer.Subscribe(KafkaTopic.ProducerAndConsumer);
        var cts = new CancellationTokenSource();

        Console.CancelKeyPress += (_
                                 , e) =>
        {
            e.Cancel = true; // prevent the process from terminating.
            cts.Cancel();
        };

        while (true)
        {
            try
            {
                var cr          = consumer.Consume(cts.Token);
                var receiveTime = DateTime.Now;
                var sendTime    = new DateTime(cr.Message.Key);
                var diffTime    = receiveTime - sendTime;

                Log.Logger.Information
                (
                    $"接收 - Topic:{KafkaTopic.ProducerAndConsumer},發送時間:{cr.Message.Value}, 接收時間：{receiveTime:s}經過時間:{diffTime.TotalSeconds}"
                );
            }
            catch (ProduceException<long, string> e)
            {
                Log.Logger.Error
                (
                    $"接收錯誤 - Topic:{KafkaTopic.ProducerAndConsumer}, Reason:{e.Error.Reason}, Error:{e.Error}"
                );
            }
        }
    }
}