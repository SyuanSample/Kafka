﻿using Confluent.Kafka;
using DomainModel.Const;
using Serilog;

namespace TestKafkaProducerAndConsumer.Unity;

public class Produce
{
    private readonly ProducerConfig _config;

    public Produce(string host)
    {
        _config = new ProducerConfig
        {
            BootstrapServers = host, SecurityProtocol = SecurityProtocol.Plaintext,
        };
    }

    private static void handler(DeliveryResult<long, string> dr)
        => Log.Logger.Information
        (
            "發送 - Topic:{ProducerAndConsumer}, Key:{DrKey}, Value:{DrValue}, Detail:{Dr}", KafkaTopic.ProducerAndConsumer, dr.Key, dr.Value, dr
        );

    public bool CreateProducer()
    {
        using var producer = new ProducerBuilder<long, string>(_config).Build();

        while (true)
        {
            try
            {
                var executeTime = DateTime.Now;
                producer.Produce
                (
                    KafkaTopic.ProducerAndConsumer
                  , new Message<long, string> {Key = executeTime.Ticks, Value = $"{executeTime:s}"}
                  , handler
                );
                
                Thread.Sleep(5000);
            }
            catch (ProduceException<long, string> e)
            {
                Log.Logger.Error
                (
                    "發送錯誤 - Topic:{ProducerAndConsumer}, Reason:{ErrorReason}, Error:{EError}", KafkaTopic.ProducerAndConsumer, e.Error.Reason, e.Error
                );
            }
        }
    }
}