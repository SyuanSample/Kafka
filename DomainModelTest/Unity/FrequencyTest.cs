﻿using DomainModel.Unity;
using FluentAssertions;
using NUnit.Framework;

namespace DomainModelTest.Unity;

public class FrequencyTest
{
    [SetUp]
    public void Setup()
    {
    }

    /// <summary>
    ///     測試每秒頻率
    /// </summary>
    [TestCase(1, 1000)]
    [TestCase(5, 200)]
    [TestCase(8, 125)]
    [TestCase(10, 100)]
    public void GetOneSecInterval_回應每秒平均值(int target
                                        , int arr)
    {
        var result = Frequency.GetOneSecInterval(target);

        result.Should()
              .Be(arr, $"預期每秒頻率為{arr}，但計算結果為${result}，結果不一致");
    }

    /// <summary>
    ///     測試每秒頻率
    /// </summary>
    [TestCase(60, 1000)]
    [TestCase(120, 500)]
    [TestCase(300, 200)]
    [TestCase(1000, 60)]
    public void GetOneMinInterval_回應每秒平均值(int target
                                        , int arr)
    {
        var result = Frequency.GetOneMinInterval(target);

        result.Should()
              .Be(arr, $"預期每秒頻率為{arr}，但計算結果為${result}，結果不一致");
    }
}