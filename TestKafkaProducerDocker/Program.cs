﻿
#region Log Area

using DomainModel.Const;
using DomainModel.Model;
using Serilog;
using TestKafkaProducerDocker;

var seqServer = Environment.GetEnvironmentVariable(EnvKey.SeqService)!;

Log.Logger = new LoggerConfiguration()
             .MinimumLevel.Information()
             .WriteTo.Seq(seqServer)
             .WriteTo.Console()
             .CreateLogger();

Log.Logger.Information("Log初始化完成，完成時間{Now}，開始主程式", DateTime.Now);

#endregion

var kafkaServer = Environment.GetEnvironmentVariable(EnvKey.KafkaServiceIpPort)!;
Log.Logger.Information("Kafka連線IP：{Kafka}", kafkaServer);

var kafka = new KafkaSetting()
{
    BootstrapServers = kafkaServer
};

var p = new Produce(kafka.BootstrapServers);
p.CreateProducer();