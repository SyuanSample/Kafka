﻿using DomainModel.Const;
using Serilog;
using ProducerConfig = Confluent.Kafka.ProducerConfig;
using SecurityProtocol = Confluent.Kafka.SecurityProtocol;

namespace TestKafkaProducerDocker;

public class Produce
{
    private readonly ProducerConfig _config;

    public Produce(string host)
    {
        _config = new ProducerConfig
        {
            BootstrapServers = host, SecurityProtocol = SecurityProtocol.Plaintext,
        };
    }

    private static void handler(Confluent.Kafka.DeliveryResult<long, string> dr)
        => Log.Logger.Information
        (
            "發送 - Topic:{ProducerAndConsumer}, Key:{DrKey}, Value:{DrValue}, Detail:{Dr}", KafkaTopic.ProducerAndConsumer, dr.Key, dr.Value, dr
        );

    public bool CreateProducer()
    {
        using var producer = new Confluent.Kafka.ProducerBuilder<long, string>(_config).Build();

        while (true)
        {
            try
            {
                var executeTime = DateTime.Now;
                producer.Produce
                (
                    KafkaTopic.ProducerAndConsumer
                  , new Confluent.Kafka.Message<long, string> {Key = executeTime.Ticks, Value = $"{executeTime:s}"}
                  , handler
                );
                
                Thread.Sleep(5000);
            }
            catch (Confluent.Kafka.ProduceException<long, string> e)
            {
                Log.Logger.Error
                (
                    "發送錯誤 - Topic:{ProducerAndConsumer}, Reason:{ErrorReason}, Error:{EError}", KafkaTopic.ProducerAndConsumer, e.Error.Reason, e.Error
                );
            }
        }
    }
}